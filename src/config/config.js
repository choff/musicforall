/**
 * config file for application
 * @module config
 */

export default {
  // path of Media Folder (something like ~/Music)
  media: '/home/choff/Music',
  data: __dirname + '/./../../data/data.json',
  scan_log: __dirname + './../../data/scanner.log',
  scan_on_init: 0
};
