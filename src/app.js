'use strict';

// Dependencies
import express from 'express';
import loki from 'lokijs';
import config from './config/config.js';
import * as musicManager from './musicManager/index';
import { restRouter } from './api/index';

// express app
const app = express();
const PORT = process.env.PORT || 3000;

// in memory database
let DB = new loki('songs.db');
let items = DB.addCollection('items');

//midlewares
app.use(express.json());
//app.use(express.urlencoded({ extended: true }));

// API router
app.use((req, res, next) => {
  console.log(req.method, ' ', req.url);
  console.log('body:', req.body);
  next();
});
app.use('/api', restRouter);


// load database and lauch server
musicManager.loadItems(config.data)
  .then(data => {
    // load data in collection 
    items.insert(data);
    // set collection in app
    app.set('items', items);
    app.set('playList', []);
    // init server
    app.listen(PORT, onListen);
  })
  .catch(err => console.log(err));

const onListen = function() {
  console.log(`listening on port ${PORT}!`);
};


