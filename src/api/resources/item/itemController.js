export default {
  findAll(req, res) {
    let items = req.app.get('items');
    //console.log('head items ', items.slice(0.3));
    let ans = items.find(obj => true);
    res.json({ data: ans });
  },
  findOne(req, res) {
    let items = req.app.get('items');
    let item = items.get(req.params.id);
    res.json({ data: item });
  }
};
