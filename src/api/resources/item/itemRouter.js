import express from 'express';
import itemController from './itemController';

export const itemRouter = express.Router();

itemRouter
  .route('/')
  .get(itemController.findAll);

itemRouter
  .route('/:id')
  .get(itemController.findOne);
