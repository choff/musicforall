import express from 'express';
import playListController from './playListController';

export const playListRouter = express.Router();

playListRouter
  .route('/')
  .get(playListController.playList)
  .post(playListController.addItem);
