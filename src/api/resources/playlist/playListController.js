export default {
  playList(req, res) {
    let playList = req.app.get('playList');
    res.json({ data: playList });
  },
  addItem(req, res) {
    let playlist = req.app.get('playList');
    console.log('playlist before ', playlist);
    playlist.push(req.body);
    console.log('playlist after ', playlist);
    console.log('in app playlist ', req.app.get('playList'));
    res.json({ data: 'item was added' });
  }
};
