import express from 'express';

import { itemRouter } from './resources/item/itemRouter';
import { playListRouter } from './resources/playlist/playListRouter';

export const restRouter = express.Router();
restRouter.use('/items', itemRouter);
restRouter.use('/playList', playListRouter);
