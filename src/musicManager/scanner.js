'use strict';

// Dependencies
import fs from 'fs';
import { exec } from 'child_process';
import config from './../config/config';

/**
 * Create a backup originalFile
 * @param {string} file - path of data.json file
 */
const copy_file = function(file) {
  return new Promise((resolve, reject) => {
    fs.copyFile(file, file + '.bak', err => {
      if (err)
        reject(err);
      resolve();
    });
  });
};

/**
 *  Exec command to scan Media folder and save results in  data.json file 
 */
const exec_cmd = function() {
  const opts = '-q -r -j -ext mp3';
  const tags = '-Artist -Album -Title';
  const output = `> ${config.media_metadata}`;
  const cmd = `exiftool ${opts} ${config.media} ${tags} ${output}`;

  return new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        //console.log('error in exec_cmd:', err);
        reject(stderr);
      }
      resolve();
    });
  });
};

/**
 * to save errors during scan process
 * @param {string} text - text with error msg
 */
const saveErrors = function(text) {
  return new Promise((resolve, reject) => {
    fs.writeFile(config.scan_log, text, err => {
      if (err) reject(err);
      let msg = `it has found few error and log that in ${config.scan_log}`;
      resolve(msg);
    });
  });
};

/** Scan Media folder to get metadata  */
const scan = function() {
  return new Promise((resolve, reject) => {
    copy_file(config.media_metadata)
      .then(exec_cmd)
      .then(() => resolve('scan commad has finished ok!!'))
      .catch(err => {
        // since some files are bad formated we'll ignore that
        // but error are save in file 
        saveErrors(err)
          .then(msg => resolve(msg))
          .catch(err => reject(err));
      });
  });
};


export {
  scan
};


// unit test run function
/*
console.time('scan');
scan()
  .then(msg => {
    console.timeEnd('scan');
    console.log(`scan results :`, msg);
  })
  .catch(err => console.log(`Runs bad status code is ${err}`));
*/
