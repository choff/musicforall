'use strict';

import fs from 'fs';
export { scan } from './scanner';

/**
 * load music_metadata.json in memory
 * @param {string} filename - path of music db file
 * @return {object} items - a loki db collection
*/
export const loadItems = function(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) reject(err);
      resolve(JSON.parse(data));
    });
  });
};
